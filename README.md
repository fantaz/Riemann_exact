# Sod shock tube calculator has moved to a new location!

If you're still interested, check out the GitLab repository [here](https://gitlab.com/fantaz/simple_shock_tube_calculator).

The [shocktubecalc](https://gitlab.com/fantaz/simple_shock_tube_calculator) is available as a PyPi package [here](https://pypi.python.org/pypi/shocktubecalc).
